;; the first symbol is the function name
;; the second symbol is the return type
;; any ^ at the start of a type refers to a pointer
;; any % at the start of an identifier refers to a dereference
(define (hi i32
	   (derp i32)
	   (foo i32)
	   (bar ^i32))
  %bar)

;; type inference
(define foo 5)
;; (define foo i32)
(define foo i32 5)

;; (fn i32 (i32 i32 i32))
(lambda (i32 (derp i32) (foo i32) (bar i32)) derp)

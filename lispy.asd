(defsystem "lispy"
  :description "lispy - a statically typed Lisp compiler"
  :version "0.0.1"
  :author "Aeron Avery <aeron.avery@cwu.edu>"
  :license "GPLv3"
  :pathname "src"
  :serial t
  :components ((:file "errors")
	       (:file "macro")
	       (:file "parser")
	       (:file "hir")
	       (:file "static-hir")
	       (:file "main")))

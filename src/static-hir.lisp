
;; (hir-type) from hir.lisp

;; name -> a string
;; type -> an hir-type
(defstruct var-decl (name) (type))

;; children -> is the scopes that have the particular scope as a parent
;; it will be used during code generation
;; generics -> a list of var-decl where the type is a function-definition
;; types -> a list of hir-type
;; variables -> a list of var-decl
(defstruct hir-scope (parent) (children) (generics) (types) (variables))

(defun new-hir-scope (parent)
  (let ((new-scope (make-hir-scope :parent parent
				   :children '()
				   :types '()
				   :variables '())))
    (setf (hir-scope-children parent) (cons new-scope (hir-scope-children parent)))
    new-scope))

(defmacro with-scope ((name) &body body)
  (let ((name (make-hir-scope :parent nil :children '() :types '() :variables '())))
    body))

(defun add-variable (scope var)
  (setf (hir-scope-variables scope) (cons var
					  (hir-scope-variables scope))))

(defun add-generic (scope generic)
  (setf (hir-scope-generics scope) (cons generic
					 (hir-scope-generics scope))))

(defun add-type (scope type)
  (setf (hir-scope-types scope) (cons type
				      (hir-scope-types scope))))

(defun is-variable-defined (scope name)
  (let ((result nil))
    (loop for var in (hir-scope-variables scope) do
	 (when (string= (var-decl-name var) name)
	   (setf result var)
	   (return)))
    (if result
	result
	(when (hir-scope-parent scope)
	  (is-variable-defined (hir-scope-parent scope) name)))))

(defun is-type-defined (scope name)
  (let ((result nil))
    (loop for type in (hir-scope-types scope) do
	 (when (eq (hir-type-name type) (hir-type-name name))
	   (setf result type)
	   (return)))
    (if result
	result
	(when (hir-scope-parent scope)
	  (is-type-defined (hir-scope-parent scope) name)))))

(defun analyze (scope node)
  (when node
    (case (hir-node-type node)
      ('define-val
       (analyze-define-val scope node))
      ('define-type-val
       (analyze-define-type-val scope node))
      ('lambda
       (analyze-lambda scope node))
      ('generic-lambda
       (analyze-generic-lambda scope node))
      ('set
       (analyze-set! scope node))
      ('if
       (analyze-if scope node))
      #|
      ;; This is only called during specific times
      ;; because only at specific times are type literals specifically created by the HIR
      ('type
       (analyze-hir-type scope node)) |#
      ('pointer
       (analyze-pointer scope node))
      ('deref
       (analyze-deref scope node))
      ('literal
       (hir-node-info node))
      ('symbol
       (analyze-symbol scope node))
      ('quote
       ;; TODO
       )
      ('func-call
       (analyze-func-call scope node)))))

(defun analyze-define-val (scope node)
  (let ((name (first (hir-node-info node)))
	(value (second (hir-node-info node))))
    (add-variable scope (make-var-decl
			 :name (hir-node-info name)
			 :type (analyze scope value)))
    (hir-type-none)))

(defun analyze-define-type-val (scope node)
  (let ((name (first (hir-node-info node)))
	(type (analyze-hir-type scope (second (hir-node-info node))))
	(value (analyze scope (third (hir-node-info node)))))
    (when (and type value)
      (unless (hir-same-type type value)
	(report-error *error-variable-type-val-different*
		      (format nil "Expected ~a got ~a."
			      (hir-type-pretty-print type)
			      (hir-type-pretty-print value))
		      (hir-node-col node) (hir-node-row node)))
      (add-variable scope (make-var-decl
			   :name (hir-node-info name)
			   :type type)))
    (hir-type-none)))

(defun analyze-lambda (scope node)
  (let ((definition (hir-node-info node))
	(local-scope (make-hir-scope :parent scope
				     :types '()
				     :variables '())))
    (let ((return-type (analyze-hir-type scope (function-definition-return-type definition)))
	  (parameter-types '()))
      ;; don't need to check return-type because it's already checked when we analyze it
      (loop for parameter in (function-definition-parameters definition) do
	   (let ((parameter-type (analyze-type-literal scope (function-parameter-type parameter))))
	     ;; make sure the parameter type is a defined type
	     (if parameter-type
		 (progn
		   ;; add parameter-type to the parameter-types list for later use
		   ;; and add the parameter to the local scope
		   (setf parameter-types (cons parameter-type
					       parameter-types))
		   (add-variable local-scope (make-var-decl
					      :name (function-parameter-name parameter)
					      :type parameter-type)))
		 (report-error *error-usage-of-undefined*
			       (format nil "Parameter type ~a is not defined as a type"
				       (hir-type-pretty-print (hir-node-info
							       (function-parameter-type parameter))))
			       (hir-node-col (function-parameter-type parameter))
			       (hir-node-row (function-parameter-type parameter))))))
      ;; analyze each body expression
      ;; since type literals are separated we don't need any special checking
      ;; (-> i32) would be treated as a function call to the function ->
      ;; if it were in the body
      (loop for body in (function-definition-body definition) do
	   (analyze local-scope body))
      
      (make-hir-type
       :name 'func
       :data `(,return-type
	       ,(reverse parameter-types))))))

;; analyze-generic-lambda retrieves the function signature from the
;; node and adds it to the current scope's generics to be used for
;; function calls.
(defun analyze-generic-lambda (scope node)
  (let ((definition (hir-node-info node)))))

(defun analyze-set! (scope node)
  ;; analyze returns the types of the expressions
  (let ((old (analyze scope (first (hir-node-info node))))
	(new (analyze scope (second (hir-node-info node)))))
    (when (and old new)
      (if (or (hir-same-type old (hir-type-none))
	      (hir-same-type new (hir-type-none)))
	  (report-error *error-improper-arg-type*
			"(set!) cannot be used with none types"
			(hir-node-col node)
			(hir-node-row node))
	  (if (not (hir-same-type old new))
	      (report-error *error-types-not-same*
			    (format nil "Expected ~a value, got ~a"
				    (hir-type-pretty-print (hir-node-info old))
				    (hir-type-pretty-print (hir-node-info new)))
			    (hir-node-col old)
			    (hir-node-row old))
	      ;; (set!) returns a none type
	      (hir-type-none))))))

(defun analyze-if (scope node)
  (let ((boolean (analyze scope (first (hir-node-info node))))
	(true (analyze scope (second (hir-node-info node))))
	(false (analyze scope (third (hir-node-info node)))))
    (if (not (hir-same-type boolean (hir-type-bool)))
	(report-error *error-improper-arg-type*
		      "The conditional expression must evaluate to a boolean"
		      (hir-node-col (first (hir-node-info node)))
		      (hir-node-row (first (hir-node-info node))))
	(if (not (hir-same-type true false))
	    (report-error *error-improper-arg-type*
			  "The true and false expressions must evaluate to the same type"
			  (hir-node-col node)
			  (hir-node-row node))
	    true))))

(defun analyze-hir-type (scope node)
  (when node
    (if (not (eq (hir-node-type node) 'type))
	(report-error *error-not-type*
		      (format nil "Expected a type got ~a"
			      (hir-node-info node))
		      (hir-node-col node) (hir-node-row node))
	(let ((type (hir-node-info node)))
	  (case (hir-type-name type)
	    ('func
	     (let ((result '()))
	       (loop for data in (hir-type-data type) do
		    (let ((defined (is-type-defined scope (hir-node-info data))))
		      (unless defined
			(report-error *error-usage-of-undefined*
				      (format nil "~a is not defined as a type."
					      (hir-type-pretty-print (hir-node-info data)))
				      (hir-node-col data) (hir-node-row data)))
		      (setf result (cons defined
					 result))))
	       (make-hir-type
		:name 'func
		:data (reverse result))))
	    ('pointer
	     (make-hir-type
	      :name 'pointer
	      :data (analyze-hir-type scope (hir-node-info node))))
	    (otherwise
	     (let ((result (is-type-defined scope type)))
	       (unless result
		 (report-error *error-usage-of-undefined*
			       (format nil "~a is not defined as a type."
				       (hir-type-pretty-print type))
			       (hir-node-col node) (hir-node-row node)))
	       result)))))))

(defun get-contained (pointer)
  (if (or (eq (hir-node-type pointer) 'pointer)
	  (eq (hir-node-type pointer) 'deref))
      (get-contained (hir-node-info pointer))
      (hir-node-info pointer)))

(defun analyze-pointer (scope node)
  (let ((contained (get-contained node)))
    (case (hir-node-type contained)
      ('symbol
       (make-hir-type
	:name 'pointer
	:data (analyze scope contained)))
      ('type
       (make-hir-type
	:name 'pointer
	:data (analyze-hir-type scope contained)))
      (otherwise
       (report-error *error-improper-pointer*
		     ;; TODO: better error message?
		     (format nil "~a does not have a memory address to access"
			     ;; TODO: pretty print
			     contained)
		     (hir-node-col contained)
		     (hir-node-row contained))))))

(defun analyze-deref (scope node)
  (let ((contained (analyze scope (get-contained node))))
    (when contained
      ;; (hir-type) here since (analyze) returns an (hir-type)
      (if (not (eq (hir-type-name contained) 'pointer))
	  (report-error *error-deref-non-pointer*
			(format nil "~a is not a pointer"
				;; TODO: pretty print
				contained)
			(hir-node-col contained)
			(hir-node-row contained))
	  ;; return the dereferenced value
	  ;; no need to analyze again since it's already been analyzed by (analyze-pointer)
	  (hir-node-info contained)))))

(defun analyze-symbol (scope node) 
  (let ((variable (is-variable-defined scope (hir-node-info node))))
    (if variable
	(var-decl-type variable)
	(report-error *error-usage-of-undefined*
		      (format nil "~a is not defined" 
			      (hir-node-info node))
		      (hir-node-col node)
		      (hir-node-row node)))))

(defun analyze-func-call (scope node)
  (let ((signature (analyze scope (function-call-name (hir-node-info node)))))
    (when signature
      ;; TODO: check for 'generic-func
      ;; needs to check for proper types with the generic names
      ;; the same generic type names must be the same types
      ;; test the generic function body with the argument types
      ;; if that passes then we can continue
      (if (not (eq (hir-type-name signature) 'func))
	  (report-error *error-defined-is-not-function*
			(format nil "~a is not a function"
				;; TODO: pretty print
				(hir-node-info (function-call-name (hir-node-info node))))
			(hir-node-col node) 
			(hir-node-row node))
	  (let ((return-type (first (hir-type-data signature)))
		(parameters (second (hir-type-data signature)))
		(arguments (function-call-arguments (hir-node-info node))))
	    ;; check for arity
	    (if (not (= (length parameters) (length arguments)))
		(report-error *error-incorrect-arity*
			      (format nil "Expected ~a arguments got ~a"
				      (length parameters)
				      (length arguments))
			      (hir-node-col node)
			      (hir-node-row node))
		;; check each argument matches the parameter
		(loop for i from 0 below (length arguments) do
		     (let ((arg-type (analyze scope (nth i arguments))))
		       (when (not (hir-same-type arg-type (nth i parameters)))
			 (report-error *error-incorrect-argument-type*
				       (format nil "Expected ~a argument type got ~a"
					       (hir-type-pretty-print (nth i parameters))
					       (hir-type-pretty-print arg-type))
				       (hir-node-col (nth i arguments))
				       (hir-node-row (nth i arguments)))
			 (return)))))
	    return-type)))))

;; define
(define-fun (define (fun return (arg type) ...) body1 body2 ...))
(define-var-val (define var value))
(define-var-type-val (define var type value))
;; lambda
(lambda (lambda (return (arg type) ...) body1 body2 ...))
;; if
(if (if expr true false))
;; boolean operators
(= (= op1 op2 op3 ...))
(< (< op1 op2 op3 ...))
(> (> op1 op2 op3 ...))
(>= (>= op1 op2 op3 ...))
(<= (<= op1 op2 op3 ...))
;; math operators
(- (- ops ...))
(+ (+ ops ...))
(* (* ops ...))
(/ (/ ops ...))
(modulo (modulo ops ...))
;; type checks
(eqv? (eqv? obj1 obj2))
(eq? (eq? obj1 obj2))
;; macros
(syntax-rules (syntax-rules (literals ...)
			     ((form1 form2 ...) output1 output2 ...) ...))
(define-syntax (define-syntax name syntax))

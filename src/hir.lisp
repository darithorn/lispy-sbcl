;; the HIR (High Intermediate Representation) is a representation that prunes the provided
;; AST so the number of unique nodes is lower. The HIR also turns the AST into more specific
;; nodes that are similar to a typical compiler AST, e.g. function call, definition, etc.

;; info [any] -> contains information specific to the node type
;; the info for a literal contains the value for the literal
;; type [symbol] -> specifies the type of the node ->
;;	   'func-call 'type 'literal 'symbol 'quote
;;         'define-val 'define-type-val 'lambda 'set 'if 'operator 'define-syntax
;;
;; the info values for each built-in are:
;; ----------------------------------
;; 'if -> `(,boolean ,true ,false)
;; 'set -> `(,variable ,value)
;; 'define-val -> `(,name ,value)
;; 'define-type-val -> `(,name ,value ,type)
;; 'lambda -> (function-definition)
;; 'operator -> (function-call)
;; 'define-syntax -> `(,name ,rules) where rules is the result of (parse-syntax-rules)
(defstruct hir-node (type) (info) (col) (row))

;; name [symbol] -> the name of the type 'func 'i32 'f32 etc.
;; data [any] -> is any additional data needed to represent the type
;;
;; the data for certain types:
;; ---------------------------
;; 'generic-func -> `(,return-type ,parameter-type ...) a list of hir-node->hir-type
;;     generic-func is used to easily distinguish from a generic and non-generic function
;; 'func -> `(,return-type ,parameter-type ...) a list of hir-node->hir-type
;; 'generic -> a quoted symbol literal
(defstruct hir-type (name) (data))

(defun hir-same-type (a b)
  (eq (hir-type-name a) (hir-type-name b)))

(defun hir-type-pretty-print (self)
  (case (hir-type-name self)
    ('func)
    (otherwise
     (hir-type-name self))))

(defun hir-type-none ()
  (make-hir-type
   :name 'none
   :data nil))

(defun hir-type-bool ()
  (make-hir-type
   :name 'boolean
   :data nil))

;; name [string] -> the name of the parameter
;; type [hir-type] -> the type of the parameter
;; optional [hir-node] -> TODO: the value the parameter defaults to if no argument
(defstruct function-parameter (name) (type) (optional))

;; return-type [hir-node] -> a hir-node with an hir-type
;; parameters [(list hir-node)] -> a list of hir-nodes with function-parameters for info
;; body [(list hir-node)] -> the body contained in the function
(defstruct function-definition (return-type) (parameters) (body))

;; name [hir-node] -> the name of the function being called
;; arguments [(list hir-node)] -> a list of the arguments passed
(defstruct function-call (name) (arguments))

(defun is-operator (name)
  (or (string= name ">")
      (string= name "<")
      (string= name "=")
      (string= name ">=")
      (string= name "<=")
      (string= name "-")
      (string= name "+")
      (string= name "*")
      (string= name "/")
      (string= name "modulo")))

;; This interprets a (form) into an (hir-node)
(defun reinterpret (form)
  (when form
      (case (form-type form)
	('list
	 ;; a function call or a polymorphic type
	 (let ((name (first (form-expr form))))
	   (cond
	     ((string= (form-expr name) "define")
	      (handle-define form))
	     ((string= (form-expr name) "if")
	      (handle-if form))
	     ((string= (form-expr name) "lambda")
	      (handle-lambda form))
	     ((string= (form-expr name) "set!")
	      (handle-set! form))
	     ((string= (form-expr name) "define-syntax")
	      (handle-define-syntax form))
	     ((string= (form-expr name) "syntax-rules")
	      (report-error *error-improper-syntax-rules-call*
			    "A call to syntax-rules must be within a define-syntax."
			    (form-col form) (form-row form)))
	     ((is-operator (form-expr name))
	      (make-hir-node
	       :type 'operator
	       :info (handle-function-call form)
	       :col (form-col form)
	       :row (form-row form)))
	     (t
	      (handle-function-call form)))))
	 ('quote
	  (reinterpret-quote form))
	 ('symbol
	  (make-hir-node
	   :type 'symbol
	   :info (form-expr form)
	   :col (form-col form)
	   :row (form-row form)))
	 ('pointer
	  (make-hir-node
	   :type 'pointer
	   :info (reinterpret (form-expr form))
	   :col (form-col form)
	   :row (form-row form)))
	 ('deref
	  (make-hir-node
	   :type 'deref
	   :info (reinterpret (form-expr form))
	   :col (form-col form)
	   :row (form-row form)))
	 (otherwise
	  (make-hir-node
	   :type 'literal
	   :info (make-hir-type
		  :name (form-type form)
		  :data (form-expr form))
	   :col (form-col form)
	   :row (form-row form))))))

(defun reinterpret-quote (form)
  (when form
    (case (form-expr (form-type form))
      ('list
       (make-hir-node
	:type 'func-call
	:info (make-function-call
	       ;; TODO: (new-list) may change names
	       :name "new-list"
	       :arguments (map 'list #'(lambda (f)
					 (reinterpret-quote f))
			       (form-expr form)))
	:col (form-col form)
	:row (form-row form)))
      ('symbol
       ;; this will just create a 'symbol (hir-node)
       (make-hir-node
	:type 'quote
	:info (reinterpret (form-expr form))
	:col (form-col form)
	:row (form-row form)))
      ('quote
       ;; having multiple quoted objects doesn't make sense
       ;; so we don't wrap it into a 'quote (hir-node)
       (reinterpret-quote (form-expr form)))
      (otherwise
       ;; quoted literals just evaluate to themselves
       (reinterpret (form-expr form))))))

;; This interprets a (form) as a (hir-type) inside an (hir-node)
;; This is needed since HIR doesn't know what is a type and what's not
;; This will tell the static analyzer that we expected a type-literal here
(defun reinterpret-type-literal (form)
  (when form
    (case (form-type form)
      ('list
       (let ((name (first (form-expr form))))
	 (cond
	   ;; there's only one type that uses a list right now
	   ;; (-> i32 (i32 i32))
	   ((string= (form-expr name) "->")
	    (let* ((return-type (second (form-expr form)))
		   (parameters (third (form-expr form)))
		   (parameter-types (if parameters
					(map 'list #'(lambda (p)
						       (reinterpret-type-literal p))
					     (form-expr parameters))
					'())))
	      (make-hir-node
	       :type 'type
	       :info (make-hir-type
		      :name 'func
		      :data (cons (reinterpret-type-literal return-type)
				  parameter-types))
	       :col (form-col form)
	       :row (form-row form))))
	   (t
	    (report-error *error-improper-type-literal*
			  "A list as a type literal is only valid for function pointers."
			  (form-col form) (form-row form))))))
      ('symbol
       (make-hir-node
	:type 'type
	:info (make-hir-type
	       ;; make into a symbol for easy comparing later on
	       :name (intern (string-upcase (form-expr form)))
	       :data nil)
	:col (form-col form)
	:row (form-row form)))
      ('pointer
       (make-hir-node
	:type 'type
	:info (make-hir-type
	       :type 'pointer
	       :info (reinterpret-type-literal (form-expr form)))
	:col (form-col form)
	:row (form-row form)))
      ('quote
       ;; used for generic  types
       (if (eq (form-type (form-expr form)) 'symbol)
	   (make-hir-node
	    :type 'type
	    :info (make-hir-type
		   :type 'generic
		   ;; turn into a symbol literal
		   :info (intern (string-upcase (form-expr (form-expr form)))))
	    :col (form-col form)
	    :row (form-row form))
	   (report-error *error-improper-type-literal*
		     (format nil "~a is not a proper type literal"
			     ;; TODO: add pretty printing to this
			     (form-expr form))
		     (form-col form) (form-row form))))
      (otherwise
       (report-error *error-improper-type-literal*
		     (format nil "~a is not a proper type literal"
			     ;; TODO: add pretty printing to this
			     (form-expr form))
		     (form-col form) (form-row form))))))

(defun expect-symbol (form fallback)
  (if form
      (unless (eq (form-type form) 'symbol)
	(report-error *error-improper-name*
		      (format nil "Names can only be symbols, got ~a" (form-expr form))
		      (form-col form) (form-row form)))
      (expect-not-nil form fallback)))

(defun expect-not-nil (form fallback &optional (msg nil))
  (unless form
    (report-error *error-improper-call-built-in*
		  (if msg msg "Expected an expression.")
		  (form-col fallback) (form-row fallback))))

(defun handle-define (form)
  (let ((second-expr (second (form-expr form))))
    (if (eq (form-type second-expr) 'list)
	(handle-define-function form)
	(if (> (length (form-expr form)) 3)
	    (handle-define-type-val form)
	    (handle-define-val form))))) 

(defun handle-define-val (form)
  (let ((name (second (form-expr form)))
	(value (third (form-expr form))))
    (expect-symbol name form)
    (make-hir-node
     :type 'define-val
     :info `(,(reinterpret name)
	      ,(reinterpret value)))))

(defun handle-define-type-val (form)
  (let ((name (second (form-expr form)))
	(type (third (form-expr form)))
	(value (fourth (form-expr form))))
    (expect-symbol name form)
    (make-hir-node
     :type 'define-type-val
     :info `(,(reinterpret name)
	      ,(reinterpret-type-literal type)
	      ,(reinterpret value))
     :col (form-col form)
     :row (form-row form))))

(defun handle-define-function (form)
  (let ((name (first (form-expr (second (form-expr form)))))
	(return-type (second (form-expr (second (form-expr form)))))
	(parameters (subseq (form-expr (second (form-expr form)))
			    2
			    (length (form-expr (second (form-expr form))))))
	(body (subseq (form-expr form)
		      2
		      (length (form-expr form))))) 
    (expect-symbol name form)
    (make-hir-node
     :type 'define-val
     :info `(,(reinterpret name)
	     ,(handle-lambda-definition form return-type parameters body))
     :col (form-col form)
     :row (form-row form))))

(defun handle-if (form)
  (let ((boolean (second (form-expr form)))
	(true (third (form-expr form)))
	(false (fourth (form-expr form))))
    (expect-not-nil boolean form)
    (expect-not-nil true form)
    (expect-not-nil false form)
    (make-hir-node
     :type 'if
     :info `(,(reinterpret boolean)
	      ,(reinterpret true)
	      ,(reinterpret false))
     :col (form-col form)
     :row (form-row form))))

(defun handle-lambda (form)
  (let* ((return-type (first (form-expr (second (form-expr form)))))
	 (parameters (rest (form-expr (second (form-expr form)))))
	 (body (subseq (form-expr form)
		       2
		       (length (form-expr form)))))
    (handle-lambda-definition form return-type parameters body)))

(defun handle-lambda-definition (form return-type parameters body)
  (expect-not-nil return-type form "A lambda requires a return type specifier")
  (unless (> (length body) 0)
    (report-error *error-improper-body*
		  "A function body requires at least one expression."
		  (form-col form) (form-col form)))
  ;; 'lambda or 'generic-lambda
  (let ((lambda-type 'lambda)
	(interpreted-return (reinterpret-type-literal return-type))
	(function-parameters '()))
    (when (eq (hir-type-data (hir-node-info interpreted-return)) 'generic)
      (setf lambda-type 'generic-lambda))
    ;; reinterpret each parameter and check if it's a generic
    (loop for pair in parameters do
	 (expect-symbol (first p) form)
	 (expect-not-nil (second p)
			 form
			 "Parameters require type specifiers after their names.")
	 (let ((name (reinterpret (first p)))
	       (type-literal (reinterpret-type-literal (second p))))
	   (when (eq (hir-type-data (hir-node-info type-literal)) 'generic)
	     (setf lambda-type 'generic-lambda))
	   (setf function-parameters (cons (make-function-parameter
					    :name name
					    :type type-literal)
					   function-parameters))))
    (make-hir-node
     :type lambda-type
     :info (make-function-definition
	    :return-type interpreted-return
	    :parameters (reverse function-parameters)
	    :body (map 'list #'(lambda (b)
				 (reinterpret b))
		       body))
     :col (form-col col)
     :row (form-row row))))

(defun handle-set! (form)
  (let ((old (second (form-expr form)))
	(new (third (form-expr form))))
    (expect-not-nil old form)
    (expect-not-nil new form)
    (make-hir-node
     :type 'set
     :info `(,(reinterpret old)
	      ,(reinterpret new))
     :col (form-col form)
     :row (form-row form))))

(defun handle-define-syntax (form)
  (let ((name (second (form-expr form)))
	(syntax (third (form-expr form))))
    (expect-symbol name form)
    (expect-not-nil syntax form "A call to (syntax-rules) must be the body")
    (make-hir-node
     :type 'define-syntax
     :info `(,(reinterpret name)
	      ,(parse-syntax-rules syntax))
     :col (form-col form)
     :row (form-row form))))

(defun handle-function-call (form)
  (let ((name (first (form-expr form)))
	(arguments (rest (form-expr form))))
    (expect-symbol name form)
    (make-hir-node
     :type 'func-call
     :info (make-function-call
	    :name (reinterpret name)
	    :arguments (map 'list #'(lambda (a)
				      (reinterpret a))
			    arguments))
     :col (form-col form)
     :row (form-row form))))

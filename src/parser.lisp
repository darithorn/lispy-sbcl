
;; type -> 'string 'list 'decimal 'integer 'symbol 'quote 'quasiquote
;; col -> the column the expression starts at
;; row -> the row the expression starts at
(defstruct form (expr) (type) (col) (row))

(defstruct lexer (file) (contents) (length) (col) (row) (i))
 
(defun new-lexer (file-name)
  (let* ((file (open file-name))
	 (length (file-length file))
	 (contents (make-string length)))
    (read-sequence contents file)
    (make-lexer
     :file file
     :contents contents
     :length length
     :col -1
     :row 1
     :i -1)))

(defun next-char (self)
  (if (>= (lexer-i self) (- (lexer-length self) 1))
      nil
      (progn
	(setf (lexer-i self) (1+ (lexer-i self)))
	(setf (lexer-col self) (1+ (lexer-col self)))
	(let ((c (char (lexer-contents self) (lexer-i self))))
	  (when (eq c #\newline)
	    (setf (lexer-row self) (1+ (lexer-row self)))
	    (setf (lexer-col self) -1))
	  c))))

(defmacro cur-char (self)
  `(char (lexer-contents ,self) (lexer-i ,self)))

(defun ahead-char (self)
  (let ((i (1+ (lexer-i self))))
    (if (< (length (lexer-contents self)) i)
	(char (lexer-contents self) (1+ (lexer-i self)))
	nil)))

(defun back-char (self)
  (setf (lexer-i self) (1- (lexer-i self)))
  (cur-char self))

(defvar *terminators* " ()[]{}#^%&\\`'.,\"")

(defun is-alpha-terminator (c)
  (or (find c *terminators*)
      (not (graphic-char-p c))))

(defun is-numeric-terminator (c)
  (or (is-alpha-terminator c)
      (not (digit-char-p c))))

(defun valid-char (c)
  (and (not (null c))
       (not (digit-char-p c))
       (or (alpha-char-p c)
	   (not (find c *terminators*)))))

(defun read-until-terminator (self &optional (is-terminator #'is-alpha-terminator))
  (let ((c (cur-char self))
	(start (lexer-i self)))
    (loop ;; #\space
       ;; hitting EOF isn't an error because EOF is considered to be a terminator
       (when (null c) (return)) ;; hit EOF
       (when (funcall is-terminator c)
	 (back-char self)
	 (return)) ;; hit another character don't want to eat it
       (setf c (next-char self)))
    (subseq (lexer-contents self) start (1+ (lexer-i self)))))

(defmacro with-lexer ((name file-name) &rest body)
  `(let ((,name (new-lexer ,file-name)))
     (setf *errors* '())
     (progn ,@body)
     (close (lexer-file ,name))))

(defun parse-symbol (lexer)
  (let ((col (lexer-col lexer))
	(row (lexer-row lexer))
	(symbol (read-until-terminator lexer)))
    (if (= 0 (length symbol))
	(progn
	  (report-error *error-critical-symbol-length-zero*
			"This is a bug in the compiler."
			col row)
	  (make-form :expr nil :type 'symbol :col col :row row))
	(make-form :expr symbol :type 'symbol :col col :row row))))

(defun parse-string (lexer)
  (let ((col (lexer-col lexer))
	(row (lexer-row lexer))
	(escaped nil)
	(c (next-char lexer)) ;; next char because cur char is "
	(start (lexer-i lexer)))
    (loop
       (when (null c)
	 (report-error *error-no-terminating-string*
		       "Expected terminating \" for string" col row)
	 (return))
       (when (and (not escaped) (eq c #\")) (return))
       (if (eq c #\\)
	   (setf escaped t)
	   (setf escaped nil))
       (setf c (next-char lexer)))
    (make-form :type 'string
	       :expr (subseq (lexer-contents lexer) start (lexer-i lexer))
	       :col col
	       :row row)))

(defun parse-number (lexer)
  (let ((col (lexer-col lexer))
	(row (lexer-row lexer))
	(number (read-until-terminator lexer #'is-numeric-terminator))
	(number-type 'integer))
    (if (eq (next-char lexer) #\.)
      (progn
	(next-char lexer)
	;; concatenate to create decimal number
	(setf number (concatenate 'string number
				  "."
				  (read-until-terminator lexer #'is-numeric-terminator)))
	(setf number-type 'decimal))
      (back-char lexer))
    (if (= 0 (length number))
	(progn
	  (report-error *error-critical-number-length-zero*
			"This is a bug in the compiler."
			col row)
	  (make-form :expr nil :type number-type :col col :row row))
	(make-form :expr number :type number-type :col col :row row))))

(defun parse-character (lexer)
  ;; (cur-char) is #
  (let ((start-col (lexer-col lexer))
	(start-row (lexer-row lexer)))
    (if (not (eq (next-char lexer) #\\))
	(progn
	  (if (or (eq (cur-char lexer) #\t)
		  (eq (cur-char lexer) #\f))
	      (make-form :type 'boolean
			 :expr (cur-char lexer)
			 :col start-col :row start-row)
	      (report-error *error-no-backslash* "Expected \\ after #" start-col start-row)))
	(let ((c (next-char lexer)))
	  (if (alpha-char-p c)
	      (progn
		;; handle explicitly typed characters like #\space #\newline #\tab 
		(let* ((col (lexer-col lexer))
		       (row (lexer-row lexer))
		       (explicit (read-until-terminator lexer))
		       (character (cond
				    ((equal explicit "space") #\space)
				    ((equal explicit "tab") #\tab)
				    ((equal explicit "newline") #\newline)
				    ;; if it's a single character
				    ((= (length explicit) 1)
				     (char explicit 0))
				    (t nil))))
		  (if (not (null character))
		      (make-form :expr (make-string 1
						    :initial-element
						    character)
				 :type 'character
				 :col start-col
				 :row start-row)
		      (report-error *error-unknown-character*
				    (format nil
					    "~a is not a valid character" explicit)
				    col row))))
	      ;; non-alphabetic characters like #\( or #\0
	      (make-form :expr (make-string 1 :initial-element c) :type 'character
			 :col start-col :row start-row))))))

;; This can parse either pointers or dereferences
;; TODO: possibly support %^foo
(defun parse-memory-handlers (lexer &optional (type 'pointer))
  (let ((col (lexer-col lexer))
	(row (lexer-row lexer))
	(f (parse-value lexer (next-char lexer)))) 
    (make-form :type type
	       :expr f
	       :col col
	       :row row)))

;; This parses an ellipsis ... used for macro variadic patterns
(defun parse-ellipsis (lexer)
  (let ((successful t)
	(i 1))
    (loop
       (if (eq (next-char lexer) #\.)
	   (progn
	     (setf i (1+ i))
	     (when (> i 3)
	       (report-error *error-ellipsis-too-many-dots*
			     "An ellipsis is only three dots, got more than three."
			     (lexer-col lexer) (lexer-row lexer))
	       (setf successful nil)
	       (return)))
	   (progn
	     (when (< i 3)
	       (report-error *error-improper-ellipsis*
			     (format nil "An ellipsis must have three dots, got ~a"
				     (cur-char lexer))
			     (lexer-col lexer) (lexer-row lexer))
	       (setf successful nil))
	     (back-char lexer)
	     (return))))
    (when successful
      (make-form :type 'ellipsis
		 :expr "..."
		 :col (lexer-col lexer)
		 :row (lexer-col lexer)))))
 
(defun parse-value (lexer c) 
  (cond
    ((eq c #\;)
     (loop
	(when (eq c #\newline) (return))
	(setf c (next-char lexer))))
    ((eq c #\()
     (parse-list lexer))
    ((eq c #\.)
     (parse-ellipsis lexer)) 
    ((valid-char c)
     (parse-symbol lexer))
    ((digit-char-p c)
     (parse-number lexer))
    ((eq c #\#)
     (parse-character lexer))
    ((eq c #\")
     (parse-string lexer))
    ((eq c #\^)
     ;; this makes it easier on the type checker to determine pointers
     (parse-memory-handlers lexer))
    ((eq c #\%)
     (parse-memory-handlers lexer 'deref))
    ((eq c #\')
     (let ((col (lexer-col lexer))
	   (row (lexer-row lexer))) 
       (make-form :expr
		  (parse-value lexer (next-char lexer))
		  :type 'quote
		  :col col
		  :row row)))
    (t
     (report-error *error-unexpected-character*
		   (format nil "Unexpected character ~a" c)
		   (lexer-col lexer) (lexer-row lexer)))))

(defun parse-list (lexer &optional (top nil))
  ;; (cur-char) is (
  (let ((new-list (list))
	(row (lexer-row lexer))
	(col (lexer-col lexer)))
    (loop
       (let ((c (next-char lexer)))
	 (loop 
	    (unless (or (eq c #\space)
			(eq c #\newline)
			(eq c #\tab)) (return))
	    (setf c (next-char lexer)))
	 (when (null c)
	   (unless top
	     ;; don't report an error if it's the top level (implicit list)
	     (report-error *error-no-terminating-paren*
			   "Unexpected end of list. Expected terminating parenthesis."
			   col row))
	   (return))
	 ;; if list terminator
	 (when (char= c #\)) (return))
	 ;; parse a value
	 (let ((tmp (parse-value lexer c)))
	   ;; is only null when there's an error while parsing
	   (unless (null tmp)
	       (setf new-list (cons tmp new-list))))))
    (make-form :expr (reverse new-list) :type 'list :col col :row row)))

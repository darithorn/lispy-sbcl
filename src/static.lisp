(defvar *built-ins* (parse-built-in-patterns "builtins.lisp"))

;; name -> the name of the actual type e.g. i32, f32, etc.
;; polymorphic-data -> any other types contained within the type e.g.
;; i32** is a type pointer to a polymorphic type pointer to an i32 type
;; another example:
;; (defstruct foo (bar i32) (foobar i32))
;; this is a type name foo containing two symbols within its polymorphic data
(defstruct declared-type (name) (polymorphic-data))

(defun new-declared-type (name &optional (polymorphic-data nil))
  (make-declared-type
   :name name
   :polymorphic-data polymorphic-data))

(defun pretty-print-function-pointer (signature)
  (format nil "(-> ~a~a)"
	  (pretty-print-declared-type (first signature))
	  (if (second signature)
	      (format nil " ~a" 
		      (loop for param in (second signature)
			 collect (pretty-print-declared-type (func-param-type param))))
	      "")))

(defun pretty-print-declared-type (type)
  (cond
    ((string= (declared-type-name type) "func")
     (pretty-print-function-pointer (declared-type-polymorphic-data type)))
    ((string= (declared-type-name type) "pointer")
     (format nil "^~a" (pretty-print-declared-type (declared-type-polymorphic-data type))))
    ((string= (declared-type-name type) "literal")
     (pretty-print-declared-type (declared-type-polymorphic-data type)))
    (t
     (declared-type-name type))))

(defun new-none () 
  (new-declared-type "none"))

(defun new-i32 ()
  (new-declared-type "i32"))

(defun new-f32 ()
  (new-declared-type "f32"))

(defun new-bool ()
  (new-declared-type "boolean"))

;; a -> a declared-type
;; b -> a declared-type 
(defun same-type (a b)
  ;; just simple strong typing for now
  ;; eventually this will become more complicated
  (if (or (null a) (null b))
      nil
      (string= (declared-type-name a) (declared-type-name b))))

(defmacro expect (form type)
  `(eq ,type (form-type ,form)))

(defmacro is-empty-form (f)
  `(= (length (form-expr ,f)) 0))

(defun handle-lambda (scope form)
  (let ((result t)
	(proper (proper-function-definition scope form))
	(local-scope (new-scope scope)))
    (when proper
      ;; compare the declared return type with the type of the last expression in the body
      (let ((last-expr-type (get-type-of-last local-scope (get-function-body form))))
	(when last-expr-type
	  (unless (same-type last-expr-type
			     (first proper))
	    (report-error *error-incorrect-return-type*
			  (format nil "lambda expected ~a return type but got ~a type for last expression."
				  (declared-type-name (first proper))
				  (declared-type-name last-expr-type))
			  ;; TODO: add col/row information that points straight to the expr
			  (form-col form) (form-row form))
	    (setf result nil))))
      (loop for param in (second proper) do
	   (add-variable local-scope (make-type-symbol :name (form-expr (func-param-name param))
						       :type (func-param-type param))))
      (loop for expr in (get-function-body form) do
	   (unless (type-of-expression local-scope expr)
	     (setf result nil))))
    (when result (new-function-type proper)))) 

(defun handle-if (scope form)
  (let ((true-type (type-of-expression scope (third (form-expr form))))
	(false-type (type-of-expression scope (fourth (form-expr form))))
	(bool-expr (type-of-expression scope (second (form-expr form)))))
    (unless (same-type bool-expr (new-bool))
      (report-error *error-if-requires-bool*
		    "The test clause must evaluate to a boolean."
		    (form-col form) (form-row form)))
    (if (same-type true-type false-type)
	true-type
	(report-error *error-types-not-same*
		      "The types of the last expressions for true and false must be the same."
		      (form-col form) (form-row form)))))

(defun handle-define-var-val (scope form)
  (let ((name (second (form-expr form)))
	(value (third (form-expr form))))
    (if (not (eq 'symbol (form-type name)))
	(report-error *error-improper-name*
		      (format nil "Got ~a expected symbol." (form-expr form))
		      (form-col name) (form-row name))
	(add-variable scope (make-type-symbol :name (form-expr name)
					      :type  (type-of-expression scope value)))))
  (new-none))

(defun handle-define-var-type-val (scope form)
  (let ((name (second (form-expr form)))
	(type (type-of-expression scope (third (form-expr form))))
	(value (type-of-expression scope (fourth (form-expr form)))))
    (if (not (eq 'symbol (form-type name)))
	(report-error *error-improper-name*
		      "Variable names must be symbols."
		      (form-col name) (form-row name))
	(if (not (same-type type value))
	    (report-error *error-variable-type-val-different*
			  (format nil "Variable definition was expecting ~a but got ~a"
				  (pretty-print-declared-type type)
				  (pretty-print-declared-type value))
			  (form-col form) (form-row form))
	    (add-variable scope (make-type-symbol :name (form-expr name)
						  :type type)))))
  (new-none))

;; this makes sure each argument is proper and adds the macro to the scope
(defun handle-define-syntax (scope form)
  (let ((name (second (form-expr form)))
	(syntax (third (form-expr form))))
    (if (not (eq 'symbol (form-type name)))
	(report-error *error-improper-name*
		      "Macro names must be symbols."
		      (form-col name) (form-row name))
	(let ((built-in (is-built-in syntax)))
	  (if (or (not built-in)
		  (not (string= (first built-in) "syntax-rules")))
	      ;; TODO: add better error reporting specifying what was wrong with the pattern
	      (report-error *error-improper-macro-body*
			    "The body of a macro definition must be (syntax-rules)"
			    (form-col syntax) (form-row syntax))
	      (let ((pattern (parse-syntax-rules syntax)))
		(when pattern
		  (add-variable scope (make-type-symbol
				       :name (form-expr name)
				       :type (new-macro-type pattern)))))))))
  (new-none)) 
 
;; Makes sure the function being called is being called correctly
;; with the correct parameter types and if the function name is actually defined
;; as a function
;; scope -> a scope
;; form -> a list-form
(defun handle-function-call (scope form)
  ;; func is a type-symbol
  (let* ((name (first (form-expr form)))
	 (signature (type-of-expression scope name)))
    ;; TODO: (type-of-expression name) to retrieve function pointer 
    ;; and use that to evaluate the function call
    (when signature
      (if (string= "macro" (declared-type-name signature))
	  (handle-macro-call scope form signature)
	  (if (not (string= "func" (declared-type-name signature)))
	      (report-error *error-defined-is-not-function*
			    (format nil "~a is not defined as a function." (form-expr name))
			    (form-col form) (form-row form))
	      ;; check each argument and match its type with the parameter type
	      (let* ((params (second (declared-type-polymorphic-data signature)))
		     (params-length (length params))
		     (args (rest (form-expr form))))
		;; functions will not be allowed to have variable argument length
		;; check arity here
		(if (not (= (length args) params-length))
		    (report-error *error-incorrect-arity*
				  (format nil "~a takes ~a arguments, got ~a"
					  (if (eq 'list (form-type name))
					      (format nil "The lambda ~a"
						      (pretty-print-function-pointer signature))
					      (form-expr name))
					  params-length (length args))
				  (form-col form) (form-row form))
		    (let ((good t))
		      (loop for i from 0 below params-length do
			   (let* ((param (nth i params))
				  (arg (nth i args))
				  (arg-type (type-of-expression scope arg)))
			     ;; analyze the argument to make sure it's proper
			     (when (type-of-expression scope arg)
			       (unless (same-type (func-param-type param)
						  arg-type) 
				 (report-error *error-incorrect-argument-type*
					       (format nil "Expected ~a got ~a type."
						       (pretty-print-declared-type (func-param-type param))
						       (pretty-print-declared-type arg-type))
					       (form-col form) (form-row form))
				 (setf good nil)))))
		      (when good (first (declared-type-polymorphic-data signature)))))))))))

;; TODO: this would be where macro expansions take place
;; when they're implemented
;; macro -> declared type with a three element list `(,name ,ident ,pattern)
(defun handle-macro-call (scope form macro)
  (let ((result nil))
    (loop for macro in (declared-type-polymorphic-data macro) do
	 (when (does-match-pattern form (third macro))
	   (setf result macro)
	   (return)))
    ;; if none of the patterns matched then we need to throw an error 
    (if (not result)
      ;; TODO: the error message should contain all the available patterns to match with
      (*error-improper-macro-call* (form-expr (first (form-expr form))) form)
      (let ((macro-name (second result)))
	(cond
	  ((string= "define-fun" macro-name)
	   (handle-function-definition scope form))
	  ((string= "define-var-val" macro-name)
	   (handle-define-var-val scope form))
	  ((string= "define-var-type-val" macro-name) 
	   (handle-define-var-type-val scope form))
	  ((string= "define-syntax" macro-name)
	   (handle-define-syntax scope form))
	  ((string= "if" macro-name)
	   (handle-if scope form))
	  ((string= "lambda" macro-name)
	   (handle-lambda scope form))
	  (t
	   ;; TODO: this is a user-defined macro
	   (new-none)))))))

;; since lambdas and defines are consistent in syntax 
;; one function can retrieve the body for both
;; This creates a list containing all the expressions in the body
;; form -> the form for the entire function definition
(defun get-function-body (form)
  (cons (third (form-expr form))
	;; this needs to retrieve the entire body
	(subseq (form-expr form) 3 (length (form-expr form))))) 

(defun get-return-type (scope form)
  (type-of-expression scope
		      (cond
			((string= "define" (form-expr (first (form-expr form))))
			 (second (form-expr
				  (second (form-expr form)))))
			((string= "lambda" (form-expr (first (form-expr form))))
			    (first (form-expr
				    (second (form-expr form))))))))

(defun get-type-of-last (scope body)
  (type-of-expression scope (nth 0 (last body))))

;; these following functions make sure function arguments and parameters match
;; and return types from the function signature and the type of the last expression match
;; form -> a list form
(defun proper-function-definition (scope form)
  (let ((return-type nil) 
	(arg-names '())
	(arg-type-names '())
	(arg-types '())
	(result t))
    (setf return-type (get-return-type scope form)) 
    ;; TODO: figure out how to deal with different indices
    ;; 'define starts at 2 while 'lambda starts at 1
    (cond
      ((string= "define" (form-expr (first (form-expr form))))
       (progn
	 (unless (eq 'symbol (form-type (first (form-expr (second (form-expr form))))))
	   (report-error *error-improper-name*
			 "A function name must be a symbol."
			 (form-col name) (form-row name)))
 	 ;; These are all hardcoded syntax values
	 ;; If the placement for parameters is changed this will break.
	 ;;(setf return-type (type-of-expression scope (second (form-expr (second (form-expr form))))))
	 ;; retrieve each argument name and type
	 (loop for i from 2 below (length (form-expr (second (form-expr form)))) do
	      (let* ((arg (nth i (form-expr (second (form-expr form)))))
		     (arg-type (second (form-expr arg))))
		(setf arg-type-names (cons arg-type
					   arg-type-names))
		(setf arg-names (cons (first (form-expr arg))
				      arg-names))))))
      ((string= "lambda" (form-expr (first (form-expr form))))
	  (progn
	    ;;(setf return-type (type-of-expression (first (form-expr (second (form-expr form))))))
	    (loop for i from 1 below (length (form-expr (second (form-expr form)))) do
		 (let* ((arg (nth i (form-expr (second (form-expr form)))))
			(arg-type (second (form-expr arg))))
		   (setf arg-type-names (cons arg-type
					      arg-type-names))
		   (setf arg-names (cons (first (form-expr arg))
					 arg-names)))))))
    ;; test whether each declared parameter type is defined
    (loop for name in arg-type-names do
	 (let ((arg-type (type-of-expression scope name)))
	   ;; add the (declared-type) to a list
	   (if arg-type
	       (progn
		 (format t "type: ~a~%" arg-type)
		 (setf arg-types (cons (declared-type-polymorphic-data arg-type) arg-types)))
	       (progn 
		 (report-error *error-usage-of-undefined*
			       (format nil "The type ~a is not defined as a type in this scope."
				       arg-type)
			       -1 -1)
		 (setf result nil)))))
    (when result
      (let ((params (turn-to-params arg-names arg-types)))
	;; we do this because if params is nil then we get a '(())
	;; since nil == '()
	(if params 
	    (list return-type params)
	    (list return-type))))))

;; this makes sure a function definition is proper
;; and makes sure each expression within the body is proper as well
;; and then adds the function definition to the current scope
;; scope -> a scope
;; form -> a list-form
(defun handle-function-definition (scope form)
  ;; proper is a list containing the return type and params
  (let ((result t)
	(proper (proper-function-definition scope form)))
    ;; we don't need to report any errors because proper-function-definition
    ;; and analyze-expression report them for us
    (when proper
      (let ((name (first (form-expr (second (form-expr form)))))
	    (params (second proper))
	    (local-scope (new-scope scope)))
	;; test if there's already a definition with the same name
	(if (is-variable-defined scope (form-expr name))
	    (report-error *error-redefinition*
			  (format nil "~a is already defined in this scope." (form-expr name))
			  (form-col name) (form-row name))
	    ;; bind the function definition to the new scope (for recursion)
	    (add-variable scope (make-type-symbol :name (form-expr name)
						  :type (new-function-type proper))))
	;; bind each parameter to the new local scope
	(loop for param in params do 
	     (add-variable local-scope (make-type-symbol :name (form-expr (func-param-name param))
							 :type (func-param-type param))))
	;; compare the declared return type with the type of the last expression in the body
	(let ((last-expr-type (get-type-of-last local-scope (get-function-body form))))
	  (when last-expr-type
	    (unless (same-type last-expr-type
			       (first proper))
	      (report-error *error-incorrect-return-type*
			    (format nil "~a expected ~a return type but got ~a type for last expression."
				    (form-expr name)
				    (pretty-print-declared-type (first proper))
				    (pretty-print-declared-type last-expr-type))
			    ;; TODO: add col/row information that points straight to the expr
			    (form-col form) (form-row form))
	      (setf result nil))))
	;; analyze each expression the body for propriety
	;; body will always have at least one expression (that's the syntax)
	(loop for expr in (get-function-body form) do
	     (let ((type (type-of-expression local-scope expr)))
	       (if type
		   (when (string= (declared-type-name type) "literal")
		     (report-error *error-improper-body*
				   (format nil "Expected expression got type literal ~a"
					   (pretty-print-declared-type
					    (declared-type-polymorphic-data type)))
				   (form-col expr) (form-row expr)))
		   (setf result nil))))))
    (when result
      (new-none))))

(defun handle-function-pointer (scope form)
  (let ((signature `(,(type-of-expression scope (second (form-expr form)))))
	(parameters (third (form-expr form))))
    (when parameters
      (loop for parameter in (form-expr parameters) do
	   (setf signature (append signature
				   (list (type-of-expression scope parameter))))))
    (new-function-type signature))) 

;; this creates a declared-type value that describes the type of the literal
;; examples of literals are: 42, 3.14, 'foo, '(foo bar), #\a
;; Lists must contain the same type for each element
;; form -> the form from the lexer/parser
(defun type-of-literal (form)
  (cond
    ((eq (form-type form) 'list)
     (if (> (length (form-expr form)) 0)
	 (let ((type-of-first (type-of-literal (first (form-expr form))))
	       (valid t))
	   ;; make sure every element in the list is the same type
	   (loop for element in (form-expr form) do
		(let ((type-of-element (type-of-literal element)))
		  (unless (same-type type-of-first type-of-element)
		    (setf valid nil)
		    (report-error *error-list-element-not-same*
				  (format nil
					  "Elements of a list must have the same type. Got type ~a, expected type ~a"
					  type-of-element type-of-first)
				  (form-col element) (form-row element)))))
	   (when valid
	     (make-declared-type :name "list"
				 :polymorphic-data type-of-first)))
	 ;; TODO: the type the list contains will eventually be inferred
	 ;; using the type of the first element that's appended to it
	 (make-declared-type :name "list"
			     :polymorphic-data (make-declared-type :name 'any
								   :polymorphic-data nil))) ) 
    ((eq (form-type form) 'integer)
     ;; integers default to i32. eventually there will be size checks to use i64
     ;; if the integer doesn't fit an i32
     (make-declared-type :name "i32" :polymorphic-data nil))
    ((eq (form-type form) 'decimal)
     ;; same as 'integer. defaults to f32, eventually dynamic size to f64
     (make-declared-type :name "f32" :polymorphic-data nil))
     ((eq (form-type form) 'quote)
     (make-declared-type :name "quote"
			 :polymorphic-data (type-of-literal (form-expr form))))
    (t
     (make-declared-type :name (string-downcase (symbol-name (form-type form))) :polymorphic-data nil)))) 

;; utility to retrieve the symbol pointers/derefs are holding
(defun get-symbol-form (form)
  (let ((s form))
    (loop
       (when (eq (form-type s) 'symbol)
	 (return))
       (setf s (form-expr form)))
    s))

;; Tests a dereference to make sure it's valid.
(defun handle-deref (scope form)
  (let* ((deref-symbol (get-symbol-form form))
	 (defined-type (is-type-defined scope (form-expr deref-symbol)))
	 (defined-variable (is-variable-defined scope (form-expr deref-symbol))))
    (if defined-type
	(report-error *error-deref-type*
		      "Types cannot be dereferenced."
		      (form-col form) (form-row form))
	;; TODO: Add a test prior to handling of dereferences that tests
	;; for proper variable definitions
	(if (null defined-variable) 
	    (report-error *error-deref-undefined-var*
			  "Unable to dereference a variable that isn't defined."
			  (form-col form) (form-row form))
	    (if (not (string= (declared-type-name (type-symbol-type defined-variable))
			      "pointer"))
		(report-error *error-deref-non-pointer*
			      "Cannot dereference non-pointer types."
			      (form-col form) (form-row form))
		(declared-type-polymorphic-data
		 (type-symbol-type defined-variable)))))))

(defun is-built-in-name (name)
  (let ((result '()))
    ;; *built-ins* is a three element list '(,name ,ident, pattern)
    ;; name is the actual name of the built-in
    ;; ident is the unique identifier to identify each macro separately
    (loop for built-in in *built-ins* do
	 (when (string= name (first built-in))
	   (setf result (cons built-in
			      result))))
    (new-macro-type result)))

;; Checks if form pattern matches a built-in form
;; form -> a list form
(defun is-built-in (form)
  (when (eq (form-type form) 'list)
    (let ((built-in (is-built-in-name (form-expr (first (form-expr form)))))
	  (result nil))
      (when built-in
	;; test if the pattern matches
	(loop for b in built-in do
	     (when (does-match-pattern form (third b))
	       (setf result `(,(second b) ,(third b)))
	       (return))))
      result)))
 
;; determines the type of an expression such as a function, a variable, or a literal
;; if the variable or function isn't defined, or there is an error, this returns nil
;; scope -> the current scope
;; form -> a form containing the expression
(defun type-of-expression (scope form)
  (cond
    ((eq (form-type form) 'list)
     ;; this is a function call
     (if (is-empty-form form)
	 (report-error *error-function-call-empty*
		       "Expected function - a non-quoted list is always a function call."
		       (form-col form) (form-row form))
	 (if (and (eq (form-type (first (form-expr form))) 'symbol)
		  ;; TODO: possibly make a separate form type
		  (string= (form-expr (first (form-expr form))) "->"))
	     (make-declared-type :name "literal"
				 :polymorphic-data (handle-function-pointer scope form))
	     (handle-function-call scope form))))
    ((eq (form-type form) 'symbol)
     ;; this is either a variable or a type
     (let ((defined-var (is-variable-defined scope (form-expr form)))
	   (defined-type (is-type-defined scope (form-expr form))))
       (if defined-type 
	   (if (null defined-var)
	       (make-declared-type :name "literal"
				   :polymorphic-data defined-type)
	       (report-error *error-defined-type-as-variable* 
			     "The names of already defined types cannot be reused as variables."
			     (form-col form) (form-row form)))
	   (if defined-var
	       ;; this will return the (declared-type) of the defined-var
	       ;; it may be a simple type or may be a function pointer or macro pattern etc.
	       (type-symbol-type defined-var)
	       ;; TODO: add support for built-ins in this context
	       ;; would provide the function type for built-ins
	       (let ((built-ins (is-built-in-name (form-expr form))))
		 (if (declared-type-polymorphic-data built-ins)
		     built-ins
		     (report-error *error-usage-of-undefined*
				   (format nil "~a is not defined within this scope." (form-expr form))
				   (form-col form) (form-row form))))))))
    ((eq (form-type form) 'pointer)
     (make-declared-type :name "pointer"
			 :polymorphic-data (type-of-expression scope (form-expr form))))
    ((eq (form-type form) 'deref)
     (handle-deref scope form))
    ;; quotes or other literals
    (t
     (type-of-literal form))))

;; symbol is also used for variable declarations
;; name -> the string name of the symbol or struct field
;; type -> a value declaring the type of the symbol e.g. i32, f32, etc.
(defstruct type-symbol (name) (type))

;; name -> the name of the parameter
;; type -> a value declaring the type of the symbol
;; options -> a list of symbols declaring options 'optional 'key 'body
;; optional-value -> only used if options contains 'optional
(defstruct func-param (name) (type) (options) (optional-value))

;; takes a list of names and types and creates a list of func-params
(defun turn-to-params (names types)
  (let ((params '()))
    (when (and names types)
      (loop for i from 0 below (length names) do
	   (setf params (cons (make-func-param
			       :name (nth i names)
			       :type (nth i types)
			       :options nil
			       :optional-value nil)
			      params))))
    params))

;; a function is declared as a type with name 'func
;; and within its polymorphic data it contains func-param for its
;; parameters and the last element is a type and is used as the return type
;; signature -> a list of `(return-type params)
(defun new-function-type (signature)
  (make-declared-type
   :name "func"
   :polymorphic-data signature))

;; Makes a new macro type to add to the scope
;; pattern -> a list of macro-pattern from (parse-syntax-rules)
(defun new-macro-type (pattern)
  (make-declared-type
   :name "macro"
   :polymorphic-data pattern))

;; gets a return type from a function's type signature
;; fn -> a declared-type
(defun retrieve-return-type (fn)
  (first (declared-type-polymorphic-data fn)))
 
;; name -> name of struct
;; fields -> list of symbols
(defun new-struct-type (name fields)
  (make-declared-type
   :name "struct"
   :polymorphic-data (cons name fields)))

;; contained-type -> the type that the pointer is pointing to
;; indirection [1] -> controls how many pointer-to-pointer types is created
(defun new-pointer-type (contained-type &optional (indirection 1))
  (if (= indirection 1)
      (make-declared-type :name "pointer" :polymorphic-data `(,contained-type))
      (make-declared-type :name "pointer" :polymorphic-data `((new-pointer-type
							      ,contained-type
							      (1- ,indirection))))))

;; The scope contains all declared-types and defined variables and functions
(defstruct scope (parent) (types) (variables))

(defun new-scope (&optional (parent nil))
  (make-scope
   :parent parent
   :types (list)
   :variables (list)))

(defmacro with-scope ((name) &body body)
  `(let ((,name (new-scope)))
     (progn 
       ,@body)))

;; defines a variabel in the specified scope
;; self -> a scope
;; var -> a type-symbol
(defun add-variable (self var)
  (setf (scope-variables self)
	(cons var
	      (scope-variables self))))

;; self -> scope
;; var -> a string representing the name of the variable
(defun is-variable-defined (self var)
  (let ((found nil))
    (loop for v in (scope-variables self) do
	 (when (string= (type-symbol-name v) var)
	   (setf found v)
	   (return)))
    (if found
	found
	(unless (null (scope-parent self))
	  (is-variable-defined (scope-parent self) var)))))

;; Adds a type to the specified scope
;; Doesn't do any checks
(defun add-type (self type)
  (setf (scope-types self)
	(cons type 
	      (scope-types self))))

;; Recursively looks for whether type is declared in the specified local scope up to the global scope
;; self -> scope
;; type -> a string representing the name of the type
(defun is-type-defined (self type)
  (let ((found nil))
    (loop for element in (scope-types self) do
	 (when (string= (declared-type-name element) type)
	   (setf found element)
	   (return)))
    (if found
	found
	(unless (null (scope-parent self))
	  (is-type-defined (scope-parent self) type)))))

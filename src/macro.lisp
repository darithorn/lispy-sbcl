;; type -> specifies the type of the sub-pattern: 'list 'symbol 'literal 'any
;;                 an 'any flag means any symbol is allowed (and is ignored) - specified by a _
;; flags -> a list of flags specifying the amount of the sub-pattern: 'ellipsis 'single
;; data -> depends on 'type':
;;                 a list will contain a list of macro-patterns
;;                 a symbol will contain the name of the symbol
;;                 a literal will contain the value of the literal
(defstruct macro-pattern (type) (flags) (data))

(defun has-flag (self flag)
  (find flag (macro-pattern-flags self)))

;; Defines a syntax-rule from (syntax-rules).
;; Contains the pattern and the output sexpr
;; pattern -> a list of macro-pattern
;; output -> the output form
(defstruct syntax-rule (pattern) (output))

(defun parse-built-in-patterns (file)
  (let ((patterns '())
	(scope (new-lexer file))) 
    (setf *errors* '())
    (let ((literals (make-form :type 'list
			       :expr '()
			       :row 0
			       :col 0))
	  (expr (parse-list scope t)))
      (loop for i from 0 below (length (form-expr expr)) do
	   (let* ((data (nth i (form-expr expr)))
		  (pattern (parse-macro-pattern (second (form-expr data)) literals))
		  (ident (form-expr (first (form-expr data))))
		  (name (form-expr (macro-pattern-data (first
							(macro-pattern-data pattern))))))
	     (setf patterns (cons `(,name ,ident ,pattern)
				  patterns)))))
    (close (lexer-file scope)) 
    patterns))

;; TODO: refactor the parameters
(defun match-form (pattern-element code-element code i)
  (let ((result t))
    ;; if the pattern element does not have an ellipsis there must be something there
    ;; since nil is a possibility we check for that
    (if (not (has-flag pattern-element 'ellipsis))
	(if (not code-element)
	    (setf result nil) 
	    (when (eq (macro-pattern-type pattern-element) 'list)
	      (setf result (does-match-pattern code-element pattern-element))))
	;; when there's code but we've reached the end of the pattern
	(if (and (null pattern-element) code-element)
	    (setf result nil)
	    (progn
	      (case (macro-pattern-type pattern-element)
		('literal
		 ;; a literal must be matched with a symbol of the same expression
		 (unless (and (eq (form-type code-element) 'symbol)
			      (string= (form-expr code-element)
				       (form-expr (macro-pattern-data pattern-element))))
		   (setf result nil))) 
		('list 
		 ;; a list pattern must be matched with a list form 
		 ;; if the list pattern has an ellipsis then every list form must match the list pattern
		 (loop for code-i from i below (length (form-expr code)) do
		    ;; match the list pattern
		      (unless (and (eq (form-type code) 'list)
				   (eq (form-type (nth code-i (form-expr code))) 'list))
			(setf result nil)
			(return))
		      (unless (does-match-pattern (nth code-i (form-expr code)) pattern-element)
			(setf result nil)
			(return))
		    ;; keep matching the list patterns if the list has an ellipsis
		      (unless (has-flag pattern-element 'ellipsis)
			(return))))
		(otherwise
		 ;; TODO: should the first symbol of the pattern be treated as a literal?
		 ;; right now this example would match true:
		 ;; (bar ((derp ...)) ...) -> matched with -> (foo ((32)) (42))
		 )))))
    result)) 

;; code -> a list form
;; pattern -> a macro-pattern
(defun does-match-pattern (code pattern)
  (let ((result t)
	(i 0))
    (loop
       (when (and (not (eq (form-type code) 'list))
		  (eq (macro-pattern-type pattern) 'list))
	 (setf result nil)
	 (return))
       (let ((pattern-element (nth i (macro-pattern-data pattern)))
	     (code-element (nth i (form-expr code))))
	 ;; if result is nil just stop matching
	 (when (or (not result)
		   ;; when both are null we've legally reached the end of the pattern
		   (and (null pattern-element) (null code-element)))
	   (return))
	 
	 (unless (match-form pattern-element code-element code i)
	   (setf result nil)
	   (return))
	 ;; if the pattern has 'al-two flag it needs to match with at least two forms
	 ;; check before the ellipsis check because as long as it matches two we don't care
	 ;; about the rest
	 #|
	 (when (has-flag pattern-element 'al-two)
	   (let ((ahead (nth (1+ i) (form-expr code))))
	     (if (null ahead)
		 (progn
		   (setf result nil)
		   (return))
		 (unless (match-form pattern-element ahead code i)
		   (setf result nil)
		   (return))))) |#
	 ;; ellipsis is always the last part of a pattern
	 (when (has-flag pattern-element 'ellipsis)
	   (return)))
       (setf i (1+ i)))
    result))

;; Parses (syntax-rules) and produces a list of '(syntax-rule ...)
;; (syntax-rules <literals> <syntax rule> ...)
;; code -> the list form  containing the entire (syntax-rules) call
(defun parse-syntax-rules (code)
  ;; (first (form-expr code)) is the symbol syntax-rules
  (let ((literals (second (form-expr code)))
	(rules '()))
    ;; no need to check for proper syntax - will be checked before this function is called
    (loop for i from 2 below (length (form-expr code)) do
	 (let* ((element (nth i (form-expr code)))
		(pattern (first (form-expr element)))
		(output (rest (form-expr element)))
		(rule (parse-macro-pattern pattern literals)))
	   (when rule
	     (setf rules (cons (make-syntax-rule
				:pattern rule
				:output output)
			       rules)))))
    rules))

(defun is-literal (s literals)
  (let ((result nil))
    (loop for l in (form-expr literals) do
	 (when (string= s (form-expr l))
	   (setf result t)
	   (return)))
    result))

(defun is-same-pattern (left right literals)
  (if (or (null left) (null right))
      nil
      (if (eq (form-type left) (form-type right))
	  (case (form-type left)
	    ('list
	     ;; if not the same length then they're obviously not the same pattern
	     (if (= (length (form-expr left)) (length (form-expr right)))
		 (let ((result t))
		   ;; if they're a list then we need to check each form in their expressions
		   (loop for i from 0 below (length (form-expr left)) do
			(unless (is-same-pattern (nth i (form-expr left))
						 (nth i (form-expr right))
						 literals)
			  (setf result nil)
			  (return)))
		   result)
		 nil))
	    ('symbol
	     ;; if one is a literal then the other must be a literal as well
	     ;; and they must be the same literal form
	     (if (is-literal (form-expr left) literals)
		 ;; if the right form has the same expression as the left form then
		 ;; the right is the same literal
		 (if (string= (form-expr left) (form-expr right))
		     t
		     nil)
		 ;; if just a symbol it doesn't matter if they're different values
		 t))
	      ;; just a catch all even though patterns can only contain symbols and lists
	    (otherwise t))
	  nil)))

;; Parses a macro pattern from (syntax-rules). Returns a macro-pattern
;; code -> the list form defining the pattern
(defun parse-macro-pattern (code literals)
  ;; TODO: add (<pattern1> ... <pattern2> ...) ability
  (if (not (eq (form-type code) 'list))
      (report-error *error-deformed-macro-pattern*
		    "A pattern must be a list."
		    (form-col code) (form-row code))
      (let ((pattern '())
	    (result t))
	(loop for i from 0 below (length (form-expr code)) do
	     (let ((element (nth i (form-expr code)))
		   (flags '()))
	       ;; look ahead to determine if there's an ellipsis
	       (let ((ahead (nth (1+ i) (form-expr code))))
		 (if (null ahead)
		     (setf flags (cons 'single flags))
		     (if (eq (form-type ahead) 'ellipsis)
			 (progn
			   (setf i (1+ i))
			   (setf flags (cons 'ellipsis flags))
			   ;; Remove the ability to do (<pattern> ... <pattern>)
			   (when (< (1+ i) (length (form-expr code)))
			     (report-error *error-deformed-macro-pattern*
					   "There cannot be any patterns after an ellipsis."
					   (form-col (nth (1+ i) (form-expr code)))
					   (form-row (nth (1+ i) (form-expr code))))
			     (setf result nil)
			     (return)))
			 (setf flags (cons 'single flags)))))
#|	       ;; check for (<pattern> ... <pattern>) 
	       (when (find 'ellipsis flags)
		 ;; if we had an ellipsis then i is currently 1+ the old i
		 (let ((ahead (nth (1+ i) (form-expr code))))
		   (unless (null ahead)
		     (setf i (1+ i))
		     (if (is-same-pattern element ahead literals)
			 ;; (<pattern> ... <pattern>) must be matched with at least two forms
			 (setf flags (cons 'al-two flags))
			 (report-error *error-illegal-pattern-after-ellipsis*
				       "A pattern after an ellipsis must match the pattern before the ellipsis."
				       (form-col ahead) (form-row ahead)))
		     ;; if we still have patterns afterwards then it's a deformed pattern
		     (when (< (1+ i) (length (form-expr code)))
		       (report-error *error-deformed-macro-pattern*
				     "There can only be one pattern after an ellipsis."
				     (form-col (nth (1+ i) (form-expr code)))
				     (form-row (nth (1+ i) (form-expr code)))))))) |#

	       
	       ;; determine the type of the pattern, create
	       ;; a macro-pattern and add it to the list of patterns
	       (case (form-type element)
		 ('symbol
		  (let ((type (if (is-literal (form-expr element) literals)
				  'literal
				  (if (string= (form-expr element) "_")
				      'any
				      'symbol))))
		    (setf pattern
			  (cons (make-macro-pattern :type type
						    :flags flags
						    :data element)
				pattern))))
		 ('list
		  (let ((list-pattern (parse-macro-pattern element literals)))
		    (setf (macro-pattern-flags list-pattern) flags)
		    (setf pattern (cons list-pattern pattern)))) 
		 ('ellipsis
		  (report-error *error-misplaced-ellipsis*
				"An ellipsis can only follow a symbol or list pattern."
				(form-col element) (form-row element))
		  (setf result nil)
		  (return))
		 (otherwise
		  (report-error *error-deformed-macro-pattern*
				"A macro pattern can only consist of symbols or lists."
				(form-col element) (form-row element))
		  (setf result nil)
		  (return)))))
	(when result
	  (make-macro-pattern :type 'list
			      :flags '(single)
			      :data (reverse pattern))))))

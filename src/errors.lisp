(defvar *error-critical-unknown-built-in* -100)

(defvar *error-critical-null-form* -3)
(defvar *error-critical-symbol-length-zero* -2)
(defvar *error-critical-number-length-zero* -1)
(defvar *error-no-terminating-paren* 1)
(defvar *error-no-backslash* 2)
(defvar *error-unexpected-character* 3)
(defvar *error-unknown-character* 4)
(defvar *error-no-terminating-string* 5)
(defvar *error-improper-memory-handler* 6)
(defvar *error-improper-ellipsis* 7)
(defvar *error-ellipsis-too-many-dots* 8)


(defvar *error-improper-arg-type* 100)
(defvar *error-list-element-not-same* 101)
(defvar *error-function-call-empty* 102)
(defvar *error-function-call-improper-name* 103)
(defvar *error-defined-type-as-variable* 104)
(defvar *error-deref-type* 105)
(defvar *error-deref-undefined-var* 106)
(defvar *error-deref-non-pointer* 107)
(defvar *error-usage-of-undefined* 108)
(defvar *error-incorrect-return-type* 109)
(defvar *error-defined-is-not-function* 110)
(defvar *error-incorrect-argument-type* 111)
(defvar *error-incorrect-arity* 112)
(defvar *error-improper-name* 113)
(defvar *error-redefinition* 114)
(defvar *error-variable-type-val-different* 115)
(defvar *error-improper-macro-body* 116)
(defvar *error-improper-macro-call* 117)
(defvar *error-if-requires-bool* 118)
(defvar *error-types-not-same* 119)
(defvar *error-improper-body* 120)
(defvar *error-missing-return-type* 121)
(defvar *error-improper-call-built-in* 122)
(defvar *error-not-type* 123)
(defvar *error-improper-pointer* 124)
(defvar *error-improper-type-literal* 125)

(defvar *error-deformed-macro-pattern* 200)
(defvar *error-misplaced-ellipsis* 201)
(defvar *error-illegal-ellipsis* 202)
(defvar *error-illegal-pattern-after-ellipsis* 203)

(defvar *errors* '()) 

(defstruct compiler-error (col) (row) (code) (msg))

(defun error->string (err)
  (format nil "[E~a] <~a:~a> ~a~%"
	  (compiler-error-code err)
	  (compiler-error-row err)
	  (compiler-error-col err)
	  (compiler-error-msg err)))

;; TODO: eventually create meaningful error codes
;; col and row are there to specify more specific col/row
;; for example: the beginning of a list if it wasn't terminated instead of the end of the file
(defun report-error (code msg col row)
  (setf *errors* (cons (make-compiler-error
			:col col
			:row row
			:code code
			:msg msg)
		       *errors*))
  nil)

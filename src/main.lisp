(with-lexer (main-lexer "test.lisp")
  ;; this will create a containing list for every expression in the file
  ;; now parsing multiple expressions is as easy as one call to parse-list
  (let ((eval-expr (parse-list main-lexer t)))
    (with-scope (global-scope)
      (add-type global-scope (make-hir-type :name 'i32 :data nil))
      (add-type global-scope (make-hir-type :name 'f32 :data nil))
      (add-variable global-scope (make-var-decl :name "bar"
						:type (make-hir-type :name 'i32 :data nil)))
      ;; (setf *built-ins* (parse-built-in-patterns "builtins.lisp"))
      (loop for expr in (form-expr eval-expr) do
	 ;;	   (format t "expr: ~a~%" expr)
	   (format t "~a~%" (analyze global-scope (reinterpret expr)))) 
      (when (> (length *errors*) 0)
	(loop for err in *errors* do
	     (format t "~a" (error->string err)))))))

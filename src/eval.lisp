(defstruct typed-value (type) (value))
; types: 'number 'string 'function 'built-in-function
(defstruct bound-var (name) (type) (value))

(defstruct lisp-fun (parameter-names) (body))

(defstruct scope (parent) (variables))
(defun new-scope (parent)
  (make-scope
   :parent parent
   :variables (list)))

(defun find-variable (self name)
  (let ((var nil))
    (loop for v in (scope-variables self) do
	 (when (string= name (bound-var-name v))
	   (setf var v)
	   (return)))
    (if (null var)
	(if (null (scope-parent self))
	    nil
	    (find-variable (scope-parent self) name))
	var)))

(defun add-variable (self var)
  (setf (scope-variables self) (cons var (scope-variables self))))

(defun eval-form (cur-scope form)
  (cond
    ((eq (form-type form) 'symbol)
     (let ((v (find-variable cur-scope (form-expr form))))
       (if (null v)
	   (progn
	     (format t "~a is not bound as a variable~%" (form-expr form))
	     nil)
	   (bound-var-value v))))
    
    ((eq (form-type form) 'list)
     ; a list expression is a list
     ; within that list are more forms that contain expressions
     (let* ((expr (form-expr form)) ; '(form ...)
	    (first-expr (first expr)) ; form
	    (fn (eval-form cur-scope first-expr))) ; typed-value
       (if (null fn)
	   (progn
	     (format t "~a is not bound to a function.~%" first-expr))
	   (if (eq (typed-value-type fn) 'built-in-function)
	       (funcall (typed-value-value  fn) (rest expr))
	       (progn
		 ; it's going to be a lisp-fun value
		 (let ((names (lisp-fun-parameter-names (typed-value-value fn)))
		       (new-scope (new-scope cur-scope)))
		   (loop for i from 1 to (1- (length names)) do
			(let ((arg (nth i expr))
			      (name (nth i names)))
			  (add-variable new-scope (make-bound-var
						   :name name
						   :type 'none
						   :value (eval-form cur-scope arg)))))
		   (eval-form new-scope (lisp-fun-body (typed-value-value fn)))))))))))

(defvar global-scope (new-scope nil))
#|(add-variable global-scope (make-bound-var
			    :name "hi"
			    :type 'number
			    :value (make-typed-value :type 'number
						    :value 10)))|#
#|(add-variable global-scope (make-bound-var
			    :name "test"
			    :type 'number
			    :value (make-typed-value :type 'built-in-function
						     :value 'test)))|#

#|(add-variable global-scope (make-bound-var
			    :name "derp"
			    :type 'function
			    :value (make-typed-value :type 'function
						     :value (make-lisp-fun
							     :parameter-names '("t")
							     :body `(,(make-form :expr 5 :type 'symbol))))))|#

(defun test (args)
  (format t "test!!!!~a~%" args)
  5)
